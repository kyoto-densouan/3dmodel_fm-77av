# README #

1/3スケールの富士通 FM-77AV風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- 富士通

## 発売時期

- FM-77AV-1/2 1985年10月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/w/index.php?title=FM-7&action=edit&section=12#FM77AV)
- [懐パソカタログ 富士通 FM77AV](http://s-sasaji.ddo.jp/pccata/fm77av.htm)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-77av/raw/0c4ad4a3a20a0547111c7f05cc1353227bbca924/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-77av/raw/0c4ad4a3a20a0547111c7f05cc1353227bbca924/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-77av/raw/0c4ad4a3a20a0547111c7f05cc1353227bbca924/ExampleImage.png)
